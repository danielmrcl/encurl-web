# encUrl
Link shortener with some extra features.

[Backend](https://gitlab.com/danielmrcl/encurl-api) in .NET minimal API.

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Compile and Minify for Production

```sh
npm run build
```
